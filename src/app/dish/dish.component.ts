import { Component, Input } from '@angular/core';

export class Dish{

  constructor(public imageUrl : string, public name : string, public half_price : number, public full_price : number, public description : string, public date: string, isMenu: boolean = true, public id: number = -1) { }

}

@Component({
  selector: 'tdm-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss']
})
export class DishComponent {

  @Input() dish: Dish;

}
