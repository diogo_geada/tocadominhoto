import { Component, OnInit, Output } from '@angular/core';
import { Dish } from '../dish/dish.component';
import { DishesService } from '../shared/menu.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Response } from 'selenium-webdriver/http';
import { LoginService, jwtKey, apiURL } from '../shared/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'tdm-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  @Output() public inputs: Dish[];
  public isLoading: boolean = false;

  constructor(public LoginManager: LoginService, public MenuManager: DishesService, public router: Router, private snackBar: MatSnackBar) {}

  ngOnInit() {

    this.MenuManager.getTodaysMenu().subscribe(data => {

      this.inputs = data;

      //the number of empty inputs to add
      let add = 2 - data.length;

      for (let i = 0; i < add; i++) {

        this.inputs.push(new Dish("", "", 0, 0, "", ""));

      }

    });



  }

  async CreateDish(dish: Dish) {

    this.isLoading = true;

    this.MenuManager.newDish(dish).subscribe(data => {

      this.isLoading = false;

      let id = Number(data.response);

      if ( id ) {

        //hide progress bar
        this.snackBar.open("Prato adicionado com sucesso", "Ok");

        this.MenuManager.getDish(id).subscribe(dish => {

          dish = dish.response[0];

          this.inputs.map((data, index) => {
            console.log(`${data.name} == ${dish.name}`);
            //Set the id in the input list so that the values can be updated instead of having to create a new dish
            if (data.name == dish.name && data.description == dish.description && data.id == -1) {

              //remove the current input and add the new, this will case the ui to rerender the forms with the correct data(id and date)
              this.inputs.splice(index, 1);
              this.inputs.push(dish);

              //if it was the first form that was changed, reoder the list
              if (index == 0) this.inputs.reverse();

              return;
            }

          });

        });

      } else {
        this.snackBar.open("Ocorreu um erro", "Ok");
        console.log(data);
      }


    }, (error) => {
      this.isLoading = false;
      console.error(error);
    });
  }


  async UpdateDish(dish: Dish) {

    this.isLoading = true;
    this.MenuManager.updateDish(dish).subscribe(data => {

      this.isLoading = false;
      if (data.response == true) this.snackBar.open("Prato alterado com sucesso!", "Ok");
      else {
        console.error(data.response);
        this.snackBar.open("Ocorreu um erro", "Ok");
      }

    });

  }

  Logout( ){
    this.LoginManager.Logout();
  }

  DownloadMenu(){
    window.open(apiURL+"/pdf", '_blank');
  }

}

export interface Response {
  response: any;
}
