import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from "@angular/material/toolbar";
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DishInputComponent } from './dish-input/dish-input.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { DishesService, MenuService } from '../shared/menu.service';
import { FormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import { AuthGuard } from '../auth.guard';

const Links = [{path: '', component: DashboardComponent }];

@NgModule({
  declarations: [ DashboardComponent, DishInputComponent ],
  imports: [
    CommonModule,
    MatToolbarModule,
    RouterModule.forChild(Links),
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  providers: [
    {provide: DishesService, useClass: MenuService}]
})
export class DashboardModule { }
