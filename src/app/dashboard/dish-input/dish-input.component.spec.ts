import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DishInputComponent } from './dish-input.component';

describe('DishInputComponent', () => {
  let component: DishInputComponent;
  let fixture: ComponentFixture<DishInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DishInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DishInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
