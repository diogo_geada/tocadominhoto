import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Dish } from 'src/app/dish/dish.component';

@Component({
  selector: 'tdm-dish-input',
  templateUrl: './dish-input.component.html',
  styleUrls: ['./dish-input.component.scss']
})
export class DishInputComponent implements OnInit {

  @Input() public dish: Dish;
  @Output() public onCreateDish: EventEmitter<Dish>;
  @Output() public onUpdateDish: EventEmitter<Dish>;


  constructor() { 
    this.onCreateDish = new EventEmitter();
    this.onUpdateDish = new EventEmitter();
  }

  ngOnInit() {
  }


  public Send(){

    //if the input fields already have a defined id, update on submit instead of creating another dish
    if(this.dish.id === -1) return this.onCreateDish.emit(this.dish);
    else return this.onUpdateDish.emit(this.dish);

  }

}
