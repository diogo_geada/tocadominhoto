import { Component, OnInit } from '@angular/core';
import { LoginService, User, jwtKey } from '../shared/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Md5 } from "ts-md5/dist/md5";
import { Router } from "@angular/router";


@Component({
  selector: 'tdm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(public loginService: LoginService, public snackbarManager: MatSnackBar, public router : Router) { }

  ngOnInit() {

  }

  public Login( user: User ){

    user.password = Md5.hashStr( user.password.toString() );

    this.loginService.Login( user ).subscribe( data => {

      if(data.response !== false){
        sessionStorage.setItem(jwtKey, data.response);
        this.router.navigate( ["/dashboard"] );

      }
      else this.snackbarManager.open("Nome de utilizador ou palavra-passe incorrectos", "Fechar")

    });
  }

}
