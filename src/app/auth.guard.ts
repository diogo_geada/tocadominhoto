import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService, jwtKey } from './shared/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _loginManager: LoginService, private _router: Router){}


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean{

    if(this._loginManager.LoggedIn()) {

      if(state.url === "/dashboard")
      {

        this._loginManager.Validate().subscribe(data => {

          if (data.response === false) {
            sessionStorage.removeItem(jwtKey);
            this._router.navigate(["/login"]);
          }

        });

      }else if(state.url === "/login"){
        this._router.navigate(["/dashboard"]);
      }
      return true;
    }
    else {

      if(state.url === "/login"){
        return true;
      }
      else{
        this._router.navigate(["/login"]);
         return false;
        }
  }}
  
}
