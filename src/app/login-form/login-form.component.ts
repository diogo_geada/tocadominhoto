import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from '../shared/login.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { validateConfig } from '@angular/router/src/config';

@Component({
  selector: 'tdm-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  public usernameInputValue: string;
  public passwordInputValue: string;

  @Output() onSubmitEvent: EventEmitter<User>;

  public validator: FormGroup;

  constructor() {
    this.onSubmitEvent = new EventEmitter<User>();

    this.validator = new FormGroup({
      "username": new FormControl(this.usernameInputValue, [
        Validators.required
      ]),
      "password": new FormControl(this.passwordInputValue,[
        Validators.required
      ])
    });

   }

  ngOnInit() {
    
  }

  public onSubmit(){

    let user: User = {username: this.usernameInputValue, password: this.passwordInputValue};
    this.onSubmitEvent.emit(user);
  }

}
