import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  private months: string[] = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]

  private GetDay(date : Date): string{
    return date.getDate().toString();
  }

  private GetMonth(date : Date): string{
    return this.months[date.getMonth()];
  }

  private GetYear(date : Date): string{
    return date.getFullYear().toString();
  }

  public GetDate(): string{
    let day = new Date();
    return `${this.GetDay(day)} de ${this.GetMonth(day)}`;
  }

}
