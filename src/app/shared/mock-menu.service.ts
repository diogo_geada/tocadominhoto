import { Injectable } from '@angular/core';
import { DishesService } from './menu.service';
import { Dish } from '../dish/dish.component';
import { Menu } from '../menu/menu.component';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class MockMenuService extends DishesService{
  getDish(id: number): Observable<any> {
    throw new Error("Method not implemented.");
  }
  updateDish(dish: Dish): Observable<any> {
    throw new Error("Method not implemented.");
  }

  newDish(dish: Dish): Observable<Dish> {
    throw new Error("Method not implemented.");
  }

  constructor(private http: HttpClient){
    super();
  }

  data_url: string = "assets/data.json";

  dishes: Dish[] = [];

  getTodaysMenu(): Observable<Dish[]> {
    
    return this.http.get<Dish[]>('/assets/data/data.json').pipe( 
      
      //map(menus => <Menu>menus.find(menu => menu.date == date))
      
    );

    
  }


}
