import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

export interface User{
  username: string;
  password: string | Int32Array;
}

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(public http: HttpClient) { }

  public Login(user: User): Observable<any>{

    return this.http.post(`${apiURL}/login`, user).pipe(
      catchError(this.handleError)
    );
  }

  public Validate(): Observable<any>{

    if(!this.LoggedIn()) return new Observable<any>();

    let data = JSON.stringify({"token": sessionStorage.getItem(jwtKey)});
    return this.http.post(`${apiURL}/login/validate`, data).pipe(
      catchError(this.handleError)
    );
  }

  public LoggedIn(): boolean{
    return !!sessionStorage.getItem(jwtKey);
  }
  
  public Logout(){
    sessionStorage.removeItem(jwtKey);
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}\n`;
    } else if(error === false){
      return  [{"response": false}];
    } {

      //if(error.status == 200) return [{"response": "username"}];
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

export const jwtKey = "tocadominhoto";
export const apiURL = "http://tocadominhoto.pt/api/public";