import { Injectable } from '@angular/core';
import { Dish } from '../dish/dish.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { apiURL } from './login.service';

export abstract class DishesService{

  abstract getTodaysMenu() : Observable<Dish[]>;
  abstract updateDish(dish: Dish) : Observable<any>;
  abstract getDish(id: number) : Observable<any>;
  abstract newDish(dish: Dish) : Observable<any>;

}

@Injectable()
export class MenuService extends DishesService{

  private httpOptions = {
    headers: new HttpHeaders({
      'Response-type' : 'json/application; charset=UTF-8',
      'Content-type' : 'json/application',
    })
  };


  constructor(private http: HttpClient){
    super();
  }

  data_url: string = "assets/data.json";

  getTodaysMenu(): Observable<Dish[]> {
    
    return this.http.get<Dish[]>(`${apiURL}/menu`);
    
  }

  public updateDish(dish: Dish): Observable<any>{

    return this.http.post(`${apiURL}/menu/dish/update`, dish, this.httpOptions).pipe(
      catchError(this.handleError)
    );
     
  }

  public getDish ( id: number ): Observable<any>{
    return this.http.post(`${apiURL}/menu/dish/${id}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  public newDish(dish: Dish, isMenu: boolean = true): Observable<any> {

    return this.http.post(`${apiURL}/menu/dish/create`, dish, this.httpOptions).pipe(
      catchError(this.handleError)
    );

  }

  private GetDay(date : Date): string{
    return (date.getDate() < 10 ? '0' : '')+date.getDate();
  }

  private GetMonth(date : Date): string{
    return (date.getMonth() < 10 ? '0' : '')+(date.getMonth()+1);
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}\n`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
