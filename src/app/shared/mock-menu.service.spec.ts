import { TestBed } from '@angular/core/testing';

import { MockMenuService } from './mock-menu.service';

describe('MockMenuervice', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockMenuService = TestBed.get(MockMenuService);
    expect(service).toBeTruthy();
  });
});
