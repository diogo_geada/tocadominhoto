import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phone'
})
export class PhonePipe implements PipeTransform {

  transform(phone: number, blockSize?: number): string {
    let phone_raw: string =  phone.toString();
    
    let limit = (blockSize) ? blockSize : 3;

    for(let i = 3; i < phone.toString().length; i+=blockSize)
    {
      phone_raw = phone_raw.substring(0, i) + ' '+ phone_raw.substring(i, phone.toString().length);
    }
    console.log(phone_raw);
    return phone_raw;
  }

}
