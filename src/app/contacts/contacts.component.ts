import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tdm-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  mapaUrl: string = "https://www.google.pt/maps/dir/current+location/Restaurante+Cervejaria+A+Toca+do+Minhoto,+R.+Dona+Isabel+de+Arag%C3%A3o+11,+2605-653+Belas,+Portugal/@38.7632751,-9.2834025,17z";
  phone: string = "214 301 840";
  constructor() { }

  ngOnInit() {
  }

}
