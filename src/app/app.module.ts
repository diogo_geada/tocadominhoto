import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ContactsComponent } from './contacts/contacts.component';
import { DishComponent } from './dish/dish.component';
import { DishesService, MenuService } from './shared/menu.service';
import { PhonePipe } from './shared/phone.pipe';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MatCardModule } from '@angular/material/card';

registerLocaleData(localePt, "pt");

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ContactsComponent,
    DishComponent,
    PhonePipe,
    HomeComponent
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MatButtonModule,
    HttpClientModule,
    MatIconModule,
    MatNativeDateModule,
    MatCardModule
  ],
  providers: [
    {provide: DishesService, useClass: MenuService},
    {provide: LocationStrategy, useClass: PathLocationStrategy},
    {provide: LOCALE_ID, useValue: "pt"}
  ],//locale id changes euro symbol to pt currency style: x€ instead of €x
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
