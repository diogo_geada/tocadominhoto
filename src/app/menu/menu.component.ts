import { Component, OnInit } from '@angular/core';
import { Dish } from '../dish/dish.component';
import { DishesService, MenuService } from '../shared/menu.service';
import { Observable } from 'rxjs';
import { DateService } from '../shared/date.service';

export interface Menu{

  date: string;
  dishes: Dish[];

}

@Component({
  selector: 'tdm-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [{provide: DishesService, useClass: MenuService}]
})
export class MenuComponent implements OnInit {

  public day : String;

  public menu$: Observable<Dish[]>;//the amount of dishes to draw

  constructor(public menuService: DishesService, public dateService: DateService) {
    this.day = this.dateService.GetDate();
   }

  ngOnInit() {

    this.menu$ = this.menuService.getTodaysMenu();
  }

  /*public DateChangedHandler (event: Date) {
    this.day = event;
    //this.GetMenu();
  }*/

}
